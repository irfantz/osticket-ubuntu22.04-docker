# Dockerfile
FROM php:8.1-apache

# Install required extensions and packages
RUN apt-get update && \
    apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
        libzip-dev \
        libonig-dev \
        libxml2-dev \
        unzip \
        wget \
        && docker-php-ext-install -j$(nproc) iconv pdo pdo_mysql mbstring gd zip xml

# Download and install osTicket
WORKDIR /var/www/html
RUN wget https://github.com/osTicket/osTicket/releases/download/v1.18.1/osTicket-v1.18.1.zip && \
    unzip osTicket-v1.18.1.zip && \
    rm osTicket-v1.18.1.zip && \
    chown -R www-data:www-data /var/www/html/upload

EXPOSE 80

CMD ["apache2-foreground"]
